# README #

Hello World!!

This is just a test project.

====

## Some basic git commands
``` bash

git config --global user.email "tin6150@gmail.com"
git config --global user.name tin6150
## need username to match what bitbucket has in record for it to prompt for pwd
git config --global credential.helper 'cache --timeout=3600'
##git config --global github.user   tin6150

git clone https://bitbucket.org/tin6150/gittest.git

git init

git add *
git commit -m "first commit"
# git remote add origin https://bitbucket.org/tin6150/gittest.git
git push -u origin master

# above code block was delimited by ``` bash  (optional language for syntax highlight)
# ends with ``` in a line by itself
```

more commands: http://psg.skinforum.org/sourceControl.html#git

------------------------------------------------------------
# Playing with markdown...

~~~~
use of 4 ~ to mark 
a code block
that spans many lines
~~~~

	tab indent will be a code block
	tabbed again

    four spaces is a code block
    another four spaces
    maybe these are for bitbucket only?


-------------------------------------------
# stock README.md below  
## the comment sign is a heading marker in markdown.  
###this is my biggest gripe about .md !!
-------------------------------------------

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
